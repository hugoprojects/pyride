import PyPR2
import math
import copy
#import os
import iksresolver
from MyPr2 import MyPr2
from Object import Object
#from Skeleton import Skeleton
import time
#import rospy
#from std_msgs.msg import String
#from std_msgs.msg import String
#Messages
#import mypkg.msg import Object
#import rospy

#iks = None
#var collection = []
NodestatusReady = True
TowerColor = []
ObjectScene = []
TemporaryObjectScene = []
GazeboPR2 = None
#Human = Skeleton()

NotifMap = {}
# NotifMap = { Eaai" : SuperposeColor }

def userLogon( name ):
  if name == 'Hugo':
    GazeboPR2.InitPosition()
  
  #PyPR2.say( '%s has logged on.' % name )

def userLogoff( name ):
  PyPR2.say( '%s has logged off.' % name )

def remoteCommandActions( cmd, arg ):
  pass

def sign(number):
  try:
    return number/abs(number)
  except ZeroDivisionError:return 0

def timerLapsedActions( id ):
  pass

def timerActions( id ):
  pass

def NodeStatusUpdate(data): 
  if (data['node'] == "Essai"):
    #print GazeboPR2.ObjectScene
    Valeurs = data['message'].split("/")
    MonNouvelObjet = Object(Valeurs[0],Valeurs[1],Valeurs[2],Valeurs[3],Valeurs[4],Valeurs[5],Valeurs[6])
    if MonNouvelObjet.ID == 0:
      GazeboPR2.ObjectScene = copy.copy(GazeboPR2.TemporaryObjectScene)
      GazeboPR2.TemporaryObjectScene = []
      GazeboPR2.TemporaryObjectScene.append(MonNouvelObjet)
    else:
      GazeboPR2.TemporaryObjectScene.append(MonNouvelObjet)

def powerPlugChangeActions( isplugged ):
  pass

def batteryChargeChangeActions( batpc, isplugged, time_remain ):
  pass

def systemShutdownActions():
  pass

def CompareObjectAndObjectScene(FirstObject,Object):
  global ObjectScene
  #for i in range(0,len(ObjectScene)):
  pass

def CreateObjectScene(data):
  #ObjetScene = Object(data[0],data[1],data[2],data[3],data[3],data[3])
  pass
def recommotCB( cmd, arg ):
  global TowerColor
  if cmd == 41:
    GazeboPR2.PickExperiment1()
  if cmd == 42:
    GazeboPR2.PickExperiment15()
  if cmd == 43:
    GazeboPR2.PickExperiment2()
  if cmd == 44:
    GazeboPR2.PickExperiment3()
  if cmd == 45:
    GazeboPR2.PickExperiment4()
  if cmd == 31:
    GazeboPR2.SuperposeColorMaster("red",0.7,0,0.60,NodestatusReady)
    #GazeboPR2.SuperposeColorMaster("blue",0.7,0,0.65,NodestatusReady) #Met l'objet un sur l'objet deux
    #GazeboPR2.HeadUp(0.4)
    print "init Position"
  if cmd == 30:
    TowerColor = arg.split("/")
    print TowerColor[0],TowerColor[1],TowerColor[2],TowerColor[3]
    #GazeboPR2.PutObjectCenter(TowerColor[0],NodestatusReady)
    if GazeboPR2.SuperposeColor(TowerColor[1],TowerColor[0],NodestatusReady): #Met l'objet un sur l'objet deux
      if GazeboPR2.SuperposeColor(TowerColor[2],TowerColor[1],NodestatusReady):
        GazeboPR2.SuperposeColor(TowerColor[3],TowerColor[2],NodestatusReady)
    #SuperposeColor('yellow','green')

def main():
  global iks
  global os
  global ObjectScene, GazeboPR2
  #rospy.init_node('MainScript')
  #
  print "Debut Programme"

  #userLogon("Hugo")
  iks = iksresolver.IKSResolver()
  GazeboPR2 = MyPr2()
  #GazeboPR2.HeadUp(0.8)
  
  #os.system('start Friend.mp3')
  #GazeboPR2.InitPosition()
  #GazeboPR2.PickObject()
  #
  PyPR2.onNodeStatusUpdate = NodeStatusUpdate
  #PyPR2.onNodeStatusUpdate = Human.NodeStatusUpdate
  PyPR2.onRemoteCommand = recommotCB
  PyPR2.onUserLogOn = userLogon
  #PyPR2.onNodeStatusUpdate = NodeStatusUpdate
  #SuperposeColor('green','blue')
  #while 1:
  
  #  Hello("arg")
  #  time.sleep(2)
  #    print "Youhou"
  #  else:
  #    print len(ObjectScene)
  #  time.sleep(2)
  #print len(ObjectScene)
  #print "Hello"  
  #while True:
    #Hello
  #  time.sleep(2)
  #  print len(ObjectScene)
  #Thread(target = func1).start()
  #Thread(target = func2).start()
  #  time.sleep(2)
    #if len(ObjectScene) != 0:
    #  print "Hello"
  
  
  #if len(ObjectScene) != 0:
  #  SuperposeColor('blue','red')
  
  #PyPR2.say( 'Hello Hugo' )
  #time.sleep(5)
  
  #iks.spr2_obj.arm_orientation('forward')
  #rospy.init_node('Main', anonymous=True)
  #rospy.Subscriber("Objects", String, callback)

  #rospy.spin()

  PyPR2.setLowPowerThreshold( 20 )

