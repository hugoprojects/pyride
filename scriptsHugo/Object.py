import PyPR2
import math
import iksresolver

class Object:
  def __init__(self,x,y,z,color,width,height,ID):
    self.x = float(x)
    self.y = float(y)
    self.z = float(z)
    self.color = str(color)
    self.width = float(width)
    self.height = float(height)
    self.ID = float(ID)
