import PyPR2
import math
import iksresolver
import time
import copy
import string
from Object import Object

class MyPr2(object):
  def __init__(self):
    self.TemporaryObjectScene = []
    self.ObjectScene = []

  def InitPosition(self):
    #POSITION WITH JOINT
    print "InitPosition"
    pose={'position':(0.6,-0.5,1),'orientation':(0.7071067811865476, 0.0, 0.7071067811865475, 0.0),'use_left_arm':False, 'wait': False}
    PyPR2.moveArmTo(**pose)
    PyPR2.setGripperPosition(3,0)
    pose={'position':(0.6,0.5,1),'orientation':(0.7071067811865476, 0.0, 0.7071067811865475, 0.0),'use_left_arm':True, 'wait': False}
    PyPR2.moveArmTo(**pose)
    #time.sleep(2)
    #time.sleep(5)

  '''def NodeStatusUpdate(self, data): #Sert a mettre mes objets decouverts dans mon environnement NODE contient le nom du node qui envoi.

    #if NotifMap.has_key( node ):
    #  NotifMap[node]( data )
    if (data['node'] == "Skeleton"):
      #print data
      DistanceMainTronc = float(data['message'])
      if DistanceMainTronc > 0.1 and DistanceMainTronc < 0.4:
        #print "Superior"
        pose={'position':(0.6,-0.5,1),'orientation':(0.7071067811865476, 0.0, 0.7071067811865475, 0.0),'use_left_arm':False}
        PyPR2.moveArmTo(**pose)
      if DistanceMainTronc > 0.4:
        self.SuperposeColor("blue","red",True)

    #print node
    if (data['node'] == "Essai"):
      #print "Environnement"
      print self.ObjectScene
      Valeurs = data['message'].split("/")
      MonNouvelObjet = Object(Valeurs[0],Valeurs[1],Valeurs[2],Valeurs[3],Valeurs[4],Valeurs[5],Valeurs[6])
      #print "".join([str(x) for x in TemporaryObjectScene])
      if MonNouvelObjet.ID == 0:
        self.ObjectScene = copy.copy(self.TemporaryObjectScene)
        self.TemporaryObjectScene = []
        self.TemporaryObjectScene.append(MonNouvelObjet)
      else:
        self.TemporaryObjectScene.append(MonNouvelObjet)'''
    
  def PickExperiment1(self):
    print "Experiment 1"
    self.PickObject2(0.56,0.26,0.68,0,0)
    #2
    #self.PickObject2(0.70,0.15,0.66,0,0)
    #3
    self.PlaceObject2(0.56,0.26,0.68,0.67,0,0.68,True)
    #self.InitPosition() # Implementer une attente du skeleton
    pose={'position':(0.6,0.5,1),'orientation':(0.7071067811865476, 0.0, 0.7071067811865475, 0.0),'use_left_arm':True}
    PyPR2.moveArmTo(**pose)
    PyPR2.setGripperPosition(1,0)
    self.PickObject2(0.56,-0.26,0.68,0,0)
    #4
    #self.PickObject(0.70,-0.35,0.67,0,0)
    #place
    #self.PlaceObject2(0.56,0.26,0.66,0.7,0,0.65,True)
    
    self.PlaceObject2(0.56,-0.26,0.68,0.69,-0.01,0.77,False)
    self.InitPosition()

  def PickExperiment15(self):
    #PyPR2.moveHeadTo(-0.03,0.4)
    self.PickObject2(0.56,0.26,0.68,0,0)
    self.PlaceObject2(0.56,0.26,0.68,0.7,0,0.68,True)
    self.InitPosition()

  def PickExperiment2(self):
    self.PickObject2(0.7,0.35,0.68,0,0)
    self.PlaceObject2(0.76,0.26,0.68,0.7,0,0.68,True)
    #time.sleep(2)
    self.InitPosition()
    PyPR2.moveHeadTo(-0.2,0.7)
    time.sleep(3)
    PyPR2.moveHeadTo(-0.03,0.4)
    time.sleep(3)
    PyPR2.moveHeadTo(-0.2,0.7)
    time.sleep(3)
    PyPR2.moveHeadTo(-0.03,0.4)
    #PyPR2.moveHeadTo(-0.8,0.5)
    #GazeboPR2.HeadUp(0.4)
    #self.PickObject2(0.56,-0.26,0.66,0,0)
  def PickExperiment3(self):
    time.sleep(2) # Il prend l'objet
    self.PickObject2(0.7,0.15,0.68,0,0)
    self.PickObject2(0.7,-0.15,0.68,0,0)
    self.PlaceObject2(0.7,0.15,0.68,0.67,0,0.68,True)
    pose={'position':(0.6,0.5,1),'orientation':(0.7071067811865476, 0.0, 0.7071067811865475, 0.0),'use_left_arm':True}
    PyPR2.moveArmTo(**pose)
    self.PlaceObject2(0.7,-0.15,0.68,0.68,0,0.73,False)
    self.InitPosition()

  def PickExperiment4(self):
    PyPR2.setGripperPosition(1,0)
    self.Gestuelle(0.85,0.35,0.80,0.5000000000000001, 0.0, 0.8660254037844386, 0.0)
    self.InitPosition()
    
  def SuperposeColorMaster(self,Object1,x,y,z,NodestatusReady):
    NodestatusReady = False
    print "Superpose"
    #print ObjectScene[0],ObjectScene[1],ObjectScene[2]
    #BoolNodesContinue = False
    BoolObject1 = False
    BoolObject2 = False 
    print "bonjour"
    for Object in self.ObjectScene:
      print Object
      if Object.color == Object1:
        BoolObject1 = True  
        MyFirstObject = Object
        print Object1
    if BoolObject1 == True:
      print "Go"
      #Use_Left_Arm = self.PickObject(MyFirstObject.x,MyFirstObject.y,MyFirstObject.z,0,0)
      Use_Left_Arm = self.PickObject(MyFirstObject.x,MyFirstObject.y,MyFirstObject.z,0,0)
      self.PlaceObject(MyFirstObject.x,MyFirstObject.y,MyFirstObject.z,x,y,z,Use_Left_Arm) 
        #GazeboPR2.InitPosition()
      NodestatusReady = True
    self.InitPosition()
    return NodestatusReady

  def PutObjectCenter(self,Object1,NodestatusReady):  
    NodestatusReady = False
    print "Put Object Center"
    for Object in self.ObjectScene:
      if Object.color == Object1:
        if Object.y < -0.2 or Object.y > 0.2:
          Use_Left_Arm = self.PickObject(Object.x,Object.y,Object.z,0,0)
          print Use_Left_Arm
          self.PlaceObject(Object.x,Object.y,Object.z,0.6,0.0,0.6,Use_Left_Arm) 
          NodestatusReady = True
          self.InitPosition()
          time.sleep(5)
    return NodestatusReady

  def SuperposeColor(self,Object1,Object2,NodestatusReady):
    Use_Left_Arm = None
    NodestatusReady = False
    print "Superpose"
    string 
    #BoolNodesContinue = False
    #print self.ObjectScene
    BoolObject1 = False
    BoolObject2 = False
    #print self.ObjectScene[0].color,self.ObjectScene[1].color
    for Object in self.ObjectScene:
      if Object.color == Object1:
        BoolObject1 = True  
        MyFirstObject = Object
        print Object1
      if Object.color == Object2:
        BoolObject2 = True
        MySecondObject = Object 
        print Object2
    if BoolObject1 == False:
      PyPR2.say("Where is the " + Object1 + " object ?")
      time.sleep(5)
    if BoolObject2 == False:
      PyPR2.say("Where is the " + Object2 + " object ?")
      time.sleep(5)
    if BoolObject2 == False or BoolObject1 == False:
      self.SuperposeColor(Object1,Object2,NodestatusReady)
    if BoolObject1 == True and BoolObject2 == True:
      print "Go"
      #Use_Left_Arm = self.PickObject(MyFirstObject.x,MyFirstObject.y,MyFirstObject.z,0,0)
      Use_Left_Arm = self.PickObject(MyFirstObject.x,MyFirstObject.y,MyFirstObject.z,0,0)
      self.PlaceObject(MyFirstObject.x,MyFirstObject.y,MyFirstObject.z,MySecondObject.x,MySecondObject.y,MySecondObject.z+0.02,Use_Left_Arm) 
        #GazeboPR2.InitPosition()
      NodestatusReady = True
    self.InitPosition()
    time.sleep(5)
    return NodestatusReady

  def Gestuelle(self,x,y,z,w,x1,y1,z1):
    Use_Left_Arm = False
    PyPR2.setGripperPosition(2,0)
    pose={'position':(x,y,z),'orientation':(w,x1,y1,z1),'use_left_arm':True}
    PyPR2.moveArmTo(**pose)
    #time.sleep(1)

  def PickObject2(self,x,y,z,width,height):
    print "Start Pick"
    if y > 0.0:
      Use_Left_Arm = True
      PyPR2.setGripperPosition(1,0.08)
      #time.sleep(2)
      pose={'position':(x,y,z+0.6),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':True}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x,y,z),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':True}
      PyPR2.moveArmTo(**pose)
      #time.sleep(1)
      PyPR2.setGripperPosition(1,0.04)
      #time.sleep(1)
      #time.sleep(3)
    else:
      Use_Left_Arm = False
      PyPR2.setGripperPosition(2,0.08)
      pose={'position':(x,y,z+0.6),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':True}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x,y,z),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':False}
      PyPR2.moveArmTo(**pose)
      #time.sleep(1)
      PyPR2.setGripperPosition(2,0.04)
      #time.sleep(1)
    return Use_Left_Arm

  def PickObject(self,x,y,z,width,height):
    print "Start Pick"
    if y > 0.0:
      Use_Left_Arm = True
      PyPR2.setGripperPosition(1,0.08)
      #time.sleep(2)
      pose={'position':(x+0.035,y-0.02,z+0.1),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':True}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x+0.035,y-0.02,z+0.06),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':True}
      PyPR2.moveArmTo(**pose)
      #time.sleep(2)
      PyPR2.setGripperPosition(1,0.04)
      time.sleep(1)
      #time.sleep(3)
    else:
      Use_Left_Arm = False
      PyPR2.setGripperPosition(2,0.08)
      pose={'position':(x+0.03,y,z+0.1),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':True}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x+0.03,y,z+0.06),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':False}
      PyPR2.moveArmTo(**pose)
      PyPR2.setGripperPosition(2,0.04)
      time.sleep(1)
    return Use_Left_Arm

  def HeadUp(self,x):
    PyPR2.moveHeadTo(0,x)

  def PlaceObject2(self,x1,y1,z1,x2,y2,z2,Use_Left_Arm):
    print "Start Place"
    if Use_Left_Arm == False:
      pose={'position':(x1,y1,z1+0.08),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x2,y2,z2+0.08),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x2,y2,z2),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      PyPR2.setGripperPosition(2,0.08)
      pose={'position':(x2,y2,z2+0.08),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      #time.sleep(1)
    else:
      pose={'position':(x1,y1,z1+0.08),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x2,y2,z2+0.08),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x2,y2,z2),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      PyPR2.setGripperPosition(1,0.08)
      pose={'position':(x2,y2,z2+0.08),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      #time.sleep(1)


  def PlaceObject(self,x1,y1,z1,x2,y2,z2,Use_Left_Arm):
    print "Start Place"
    if Use_Left_Arm == False:
      pose={'position':(x1+0.035,y1,z1+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      bool1 = PyPR2.moveArmTo(**pose)
      pose={'position':(x2+0.05,y2-0.02,z2+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      bool2 =PyPR2.moveArmTo(**pose)
      pose={'position':(x2+0.05,y2-0.02,z2+0.08),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      bool3 =PyPR2.moveArmTo(**pose)
      PyPR2.setGripperPosition(2,0.08)
      pose={'position':(x2+0.05,y2-0.02,z2+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      bool4 =PyPR2.moveArmTo(**pose)
      if bool4 == False:
        PyPR2.say("Help Please. I need a friend...")
      #time.sleep(1)
    else:
      pose={'position':(x1+0.03,y1,z1+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x2+0.03,y2-0.02,z2+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x2+0.03,y2-0.02,z2+0.08),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      PyPR2.setGripperPosition(1,0.08)
      pose={'position':(x2+0.03,y2-0.02,z2+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      bool4 =PyPR2.moveArmTo(**pose)
      if bool4 == False:
        PyPR2.say("Help Please. I need a friend ...")
      #time.sleep(1)'''


