import PyPR2
import math
import iksresolver
import time
import copy
import string
from Object import Object

class MyPr2(object):
  def __init__(self):
    self.TemporaryObjectScene = []
    self.ObjectScene = []

  def InitPosition(self):
    #POSITION WITH JOINT
    print "InitPosition"
    pose={'position':(0.6,-0.5,1),'orientation':(0.7071067811865476, 0.0, 0.7071067811865475, 0.0),'use_left_arm':False, 'wait': False}
    PyPR2.moveArmTo(**pose)
    PyPR2.setGripperPosition(3,0)
    pose={'position':(0.6,0.5,1),'orientation':(0.7071067811865476, 0.0, 0.7071067811865475, 0.0),'use_left_arm':True, 'wait': False}
    PyPR2.moveArmTo(**pose)

  def PutObjectCenter(self,Object1,NodestatusReady):  
    NodestatusReady = False
    print "Put Object Center"
    for Object in self.ObjectScene:
      if Object.color == Object1:
        if Object.y < -0.2 or Object.y > 0.2:
          Use_Left_Arm = self.PickObject(Object.x,Object.y,Object.z,0,0)
          print Use_Left_Arm
          self.PlaceObject(Object.x,Object.y,Object.z,0.6,0.0,0.6,Use_Left_Arm) 
          NodestatusReady = True
          self.InitPosition()
          time.sleep(5)
    return NodestatusReady

  def SuperposeColor(self,Object1,Object2,NodestatusReady):
    Use_Left_Arm = None
    NodestatusReady = False
    print "Superpose"
    string 
    #BoolNodesContinue = False
    #print self.ObjectScene
    BoolObject1 = False
    BoolObject2 = False
    #print self.ObjectScene[0].color,self.ObjectScene[1].color
    for Object in self.ObjectScene:
      if Object.color == Object1:
        BoolObject1 = True  
        MyFirstObject = Object
        print Object1
      if Object.color == Object2:
        BoolObject2 = True
        MySecondObject = Object 
        print Object2
    if BoolObject1 == False:
      PyPR2.say("Where is the " + Object1 + " object ?")
      time.sleep(5)
    if BoolObject2 == False:
      PyPR2.say("Where is the " + Object2 + " object ?")
      time.sleep(5)
    if BoolObject2 == False or BoolObject1 == False:
      self.SuperposeColor(Object1,Object2,NodestatusReady)
    if BoolObject1 == True and BoolObject2 == True:
      print "Go"
      #Use_Left_Arm = self.PickObject(MyFirstObject.x,MyFirstObject.y,MyFirstObject.z,0,0)
      Use_Left_Arm = self.PickObject(MyFirstObject.x,MyFirstObject.y,MyFirstObject.z,0,0)
      self.PlaceObject(MyFirstObject.x,MyFirstObject.y,MyFirstObject.z,MySecondObject.x,MySecondObject.y,MySecondObject.z+0.02,Use_Left_Arm) 
        #GazeboPR2.InitPosition()
      NodestatusReady = True
    self.InitPosition()
    time.sleep(5)
    return NodestatusReady

  def PickObject(self,x,y,z,width,height):
    print "Start Pick"
    if y > 0.0:
      Use_Left_Arm = True
      PyPR2.setGripperPosition(1,0.08)
      #time.sleep(2)
      pose={'position':(x+0.035,y-0.02,z+0.1),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':True}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x+0.035,y-0.02,z+0.06),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':True}
      PyPR2.moveArmTo(**pose)
      #time.sleep(2)
      PyPR2.setGripperPosition(1,0.04)
      time.sleep(1)
      #time.sleep(3)
    else:
      Use_Left_Arm = False
      PyPR2.setGripperPosition(2,0.08)
      pose={'position':(x+0.03,y,z+0.1),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':True}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x+0.03,y,z+0.06),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':False}
      PyPR2.moveArmTo(**pose)
      PyPR2.setGripperPosition(2,0.04)
      time.sleep(1)
    return Use_Left_Arm

  def HeadUp(self,x):
    PyPR2.moveHeadTo(0,x)

  def PlaceObject(self,x1,y1,z1,x2,y2,z2,Use_Left_Arm):
    print "Start Place"
    if Use_Left_Arm == False:
      pose={'position':(x1+0.035,y1,z1+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      bool1 = PyPR2.moveArmTo(**pose)
      pose={'position':(x2+0.05,y2-0.02,z2+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      bool2 =PyPR2.moveArmTo(**pose)
      pose={'position':(x2+0.05,y2-0.02,z2+0.08),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      bool3 =PyPR2.moveArmTo(**pose)
      PyPR2.setGripperPosition(2,0.08)
      pose={'position':(x2+0.05,y2-0.02,z2+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      bool4 =PyPR2.moveArmTo(**pose)
      #if bool1 == False or bool2 == False or bool3 == False or bool4 == False :
      #  PyPR2.say("Help Please")
      #time.sleep(1)
    else:
      pose={'position':(x1+0.03,y1,z1+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x2+0.03,y2-0.02,z2+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x2+0.03,y2-0.02,z2+0.08),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      PyPR2.setGripperPosition(1,0.08)
      pose={'position':(x2+0.03,y2-0.02,z2+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      #time.sleep(1)'''


