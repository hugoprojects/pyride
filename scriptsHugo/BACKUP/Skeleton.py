import PyPR2
import math
import iksresolver
import time
from Entite import Entite

class Skeleton:
  def __init__(self,right_hand,torso):
    self.right_hand = Entite(right_hand.x,right_hand.y,right_hand.z)
    self.torso = Entite(torso.x,torso.y,torso.z)

