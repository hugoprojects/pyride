import PyPR2
import math
import iksresolver
import time

class MyPr2(object):
  def __init__(self):
    pass

  def InitPosition(self):
    #POSITION WITH JOINT
    print "InitPosition"
    '''joint={'l_wrist_flex_joint': 1.7,'l_wrist_roll_joint': 1.7, 'l_forearm_roll_joint': 0, 'l_elbow_flex_joint': -1, 'l_shoulder_lift_joint': 0.8, 'l_upper_arm_roll_joint': 1,  'l_shoulder_pan_joint': 0.85 }    
    PyPR2.moveArmWithJointPos(**joint)
    joint={'r_wrist_flex_joint': 1.7,'r_wrist_roll_joint': 1.7, 'r_forearm_roll_joint': 0, 'r_elbow_flex_joint': -1, 'r_shoulder_lift_joint': 0.8, 'r_upper_arm_roll_joint': -1,  'r_shoulder_pan_joint': -0.85 }    
    PyPR2.moveArmWithJointPos(**joint)'''
    #POSITION WITH POSE
    #PyPR2.moveTorsoBy(0.2,100)
    #PyPR2.moveHeadTo(0,0.7)
    pose={'position':(0.6,-0.5,1),'orientation':(0.7071067811865476, 0.0, 0.7071067811865475, 0.0),'use_left_arm':False}
    PyPR2.moveArmTo(**pose)
    pose={'position':(0.6,0.5,1),'orientation':(0.7071067811865476, 0.0, 0.7071067811865475, 0.0),'use_left_arm':True}
    PyPR2.moveArmTo(**pose)
    time.sleep(5)
    #time.sleep(5)
    #pass

  def PickObject(self,x,y,z,width,height):
    print "Start Pick"
    if y > 0.0:
      Use_Left_Arm = True
      PyPR2.setGripperPosition(1,0.08)
      #time.sleep(2)
      pose={'position':(x+0.035,y,z+0.1),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':True}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x+0.035,y,z+0.06),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':True}
      PyPR2.moveArmTo(**pose)
      #time.sleep(2)
      PyPR2.setGripperPosition(1,0.04)
      time.sleep(1)
      #time.sleep(3)
    else:
      #print "Droite"
      #PAR LE DEVANT
      '''Use_Left_Arm = False
      PyPR2.setGripperPosition(2,0.08)
      pose={'position':(x-0.05,y,z+0.05),'orientation':(0.5000000000000001, 0.0, 0.8660254037844386, 0.0),'use_left_arm':False}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x,y,z),'orientation':(0.7071067811865476, 0.0, 0.7071067811865475, 0.0),'use_left_arm':False}
      PyPR2.moveArmTo(**pose)
      PyPR2.setGripperPosition(2,0.03)'''
      #PAR LE DESSUS
      Use_Left_Arm = False
      PyPR2.setGripperPosition(2,0.08)
      #time.sleep(2)
      #print x
      pose={'position':(x+0.035,y,z+0.06),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':False}
      PyPR2.moveArmTo(**pose)
      #time.sleep(2)
      #time.sleep(4)
      #pose={'position':(x,y,z+0.01),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':False}
      #print x,y,z
      #PyPR2.moveArmTo(**pose)
      #time.sleep(2)
      PyPR2.setGripperPosition(2,0.04)
      time.sleep(1)
      #time.sleep(3)
    #time.sleep(5)
    return Use_Left_Arm

  def PlaceObject(self,x1,y1,z1,x2,y2,z2,Use_Left_Arm):
    print "Start Place"
    #poses = []
    #poses.append({'position':(0.63,0,0.7),'orientation':(1.0 , 0.0, 0.0, 0.0)})
    #poses.append({'position':(0.5,0,0.8),'orientation':(1.0 , 0.0, 0.0, 0.0)}
    #poses.append({'position':(0.63,0,0.6),'orientation':(1.0 , 0.0, 0.0, 0.0)})
    #poses.append({'position':(0.80,0,0.7),'orientation':(1.0 , 0.0, 0.0, 0.0)})
    #PyPR2.moveArmInTrajectory( traj = poses, time = 10.0, left_arm =  Use_Left_Arm, relative = False )
    #pose={'position':(x1,y1,1),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':False}
    #PyPR2.moveArmTo(**pose)
    if Use_Left_Arm == False:
      pose={'position':(x1+0.035,y1,z1+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x2+0.05,y2-0.02,z2+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x2+0.05,y2-0.02,z2+0.08),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      PyPR2.setGripperPosition(2,0.08)
      pose={'position':(x2+0.05,y2-0.02,z2+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      time.sleep(1)
    else:
      pose={'position':(x1+0.03,y1,z1+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x2+0.03,y2-0.02,z2+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      pose={'position':(x2+0.03,y2-0.02,z2+0.08),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      PyPR2.setGripperPosition(1,0.08)
      pose={'position':(x2+0.03,y2-0.02,z2+0.16),'orientation':(0.008726535498373897, 0.0, 0.9999619230641713, 0.0),'use_left_arm':Use_Left_Arm}
      PyPR2.moveArmTo(**pose)
      time.sleep(1)
